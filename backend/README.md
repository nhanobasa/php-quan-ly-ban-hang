# backend

CNPM backend

## API User
- API đăng nhập POST /api/login ok
- API đăng ký POST /api/signup ok
## API Customers
- API hiển thị danh sách khách hàng GET /api/customer ok
- API thêm khách hàng mới POST /api/addCustomer ok
- API sửa khách hàng PUT /api/editCustomer ok
- API xóa khách hàng DELETE /api/delCustomer ok
## API Products