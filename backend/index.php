<?php
//      Allow CORS

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

require_once "vendor/autoload.php";
require_once 'app/Controllers/UserApi.php';
require_once 'app/Controllers/CustomerApi.php';
require_once 'app/Controllers/CategoryApi.php';
require_once 'app/Controllers/ProductApi.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();


$userApi = new UserApi();
$customerApi =  new Customer_API();
$categoryApi = new Category_API();
$productApi = new Product_API();

http_response_code(400);
echo json_encode(array("msg" => "Unknow endpoint."));
