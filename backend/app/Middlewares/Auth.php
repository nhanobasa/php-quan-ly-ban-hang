<?php

require 'vendor/autoload.php';

use Firebase\JWT\JWT;


class Auth
{
    public static function authMiddleware($roles = [])
    {
        $headers = apache_request_headers();
        if (isset($headers["Authorization"])) {
            $arr = explode(" ", $headers["Authorization"]);
            $jwt = $arr[1];
            if ($jwt) {
                try {
                    $decoded = JWT::decode($jwt, $_ENV["ACCESSTOKENKEY"], array('HS256'));
                    
                    $data =  (array) $decoded->data;
                    $checkRole = 0;
                    if (count($roles)) {
                        foreach ($roles as $role) {
                            if ($data["role"] == $role) {
                                $checkRole++;
                            }
                        }
                        if (!$checkRole) {
                            return array("error" => "Unauthorized");
                        }
                    }                    
                    return $data;
                } catch (Exception $e) {
                    return array("error" => $e->getMessage());
                }
            }
        } else {
            return array("error" => "Unauthorized");
        }
    }
}
