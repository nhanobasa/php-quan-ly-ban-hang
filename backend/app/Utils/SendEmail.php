<?php

require_once('app/Utils/PHPMailer/src/PHPMailer.php');
require_once('app/Utils/PHPMailer/src/Exception.php');
require_once('app/Utils/PHPMailer/src/OAuth.php');
require_once('app/Utils/PHPMailer/src/POP3.php');
require_once('app/Utils/PHPMailer/src/SMTP.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SendEmail
{
    /**
     * @param string $title "Tiêu đề Email"
     * @param string $content Nội dung email
     * @param string $nTo Tên người nhận email
     * @param string $mto Địa chỉ nhận Email
     * @param string $diachicc gửi đến nhiều người
     * @return integer 0 Nếu như gửi lỗi
     * @return integer 1 Nếu như gửi thành cồng
     */
    public static function sendMail($title, $content, $nTo, $mTo, $diachicc = '')
    {

        $nFrom = 'DENOSHOP';
        $mFrom = 'nhantd99@gmail.com';  //dia chi email cua ban 
        $mPass = 'Matkhau@1006';       //mat khau email cua ban
        $mail             = new PHPMailer();
        $body             = $content;
        $mail->IsSMTP();
        $mail->CharSet   = "utf-8";
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth   = true;                    // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = "smtp.gmail.com";
        $mail->Port       = 465;
        $mail->Username   = $mFrom;  // GMAIL username
        $mail->Password   = $mPass;               // GMAIL password
        $mail->SetFrom($mFrom, $nFrom);
        //chuyen chuoi thanh mang
        $ccmail = explode(',', $diachicc);
        $ccmail = array_filter($ccmail);
        if (!empty($ccmail)) {
            foreach ($ccmail as $k => $v) {
                $mail->AddCC($v);
            }
        }
        $mail->Subject    = $title;
        $mail->MsgHTML($body);
        $address = $mTo;
        $mail->AddAddress($address, $nTo);
        $mail->AddReplyTo($mFrom, 'DENOSHOP');
        $mail->Send();
        if (!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * @param string $title "Tiêu đề Email"
     * @param string $content Nội dung email
     * @param string $nTo Tên người nhận email
     * @param string $mto Địa chỉ nhận Email
     * @param string $diachicc gửi đến nhiều người
     * @param string $file File muốn gửi kèm
     * @param string $filename Tên file gửi kèm
     * @return integer 0 Nếu như gửi lỗi
     * @return integer 1 Nếu như gửi thành cồng
     */
    public static function sendMailAttachment($title, $content, $nTo, $mTo, $diachicc = '', $file, $filename)
    {
        $nFrom = 'DENOSHOP';
        $mFrom = 'nhantd99@gmail.com';  //dia chi email cua ban 
        $mPass = 'Matkhau@1006';       //mat khau email cua ban
        $mail             = new PHPMailer();
        $body             = $content;
        $mail->IsSMTP();
        $mail->CharSet   = "utf-8";
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth   = true;                    // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = "smtp.gmail.com";
        $mail->Port       = 465;
        $mail->Username   = $mFrom;  // GMAIL username
        $mail->Password   = $mPass;               // GMAIL password
        $mail->SetFrom($mFrom, $nFrom);
        //chuyen chuoi thanh mang
        $ccmail = explode(',', $diachicc);
        $ccmail = array_filter($ccmail);
        if (!empty($ccmail)) {
            foreach ($ccmail as $k => $v) {
                $mail->AddCC($v);
            }
        }
        $mail->Subject    = $title;
        $mail->MsgHTML($body);
        $address = $mTo;
        $mail->AddAddress($address, $nTo);
        $mail->AddReplyTo('info@DENOSHOP', 'DENOSHOP');
        $mail->AddAttachment($file, $filename);
        if (!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }
}
