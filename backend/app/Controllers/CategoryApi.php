<?php

/**
 * CategoryApi Controllers
 */
require_once 'app/Utils/RestfulAPI.php';
require_once 'app/Middlewares/Auth.php';
require_once 'app/config/Database.php';
require_once 'app/Models/Category.php';


class Category_API extends Restful_API
{
    private $database;
    private $db;
    private $category;
    function __construct()
    {
        $this->database = new Database();
        $this->db = $this->database->getConnection();
        $this->category = new Category($this->db);
        parent::__construct();
    }

    function category()
    {
        // Auth Middleware
        $role = [$_ENV["ROLE_NVQLK"], $_ENV["ROLE_ADMIN"]];
        $authMid = Auth::authMiddleware($role);
        if (isset($authMid["error"])) {
            $this->response(401, $authMid);
        }

        if ($this->method == "GET") {
            // Nếu là method GET -> lấy danh sách category
            $category_id = isset($_GET["category_id"]) ? $_GET["category_id"] : null;
            if ($category_id != "") {
                // Nếu như có category_id !== "" -> Lấy thông tin category (bao gồm cả danh sách product tương ứng).
                $this->category->setCategory_id($category_id);
                $category_arr = $this->category->getProductsOfCategory();
                if (isset($category_arr["body"])) {
                    $this->response(200, $category_arr);
                } else
                    $this->response(500, $category_arr);
            } else if ($category_id === "") {
                $this->response(500, array("error" => "No Category ID provided."));
            } else {
                // Khi category_id = null -> Lấy toàn bộ danh sách thông tin category (không bao gồm danh product tương ứng).
                $category_arr = $this->category->getAllCategories();
                if (isset($category_arr["body"])) {
                    $this->response(200, $category_arr);
                } else
                    $this->response(500, $category_arr);
            }
        } else if ($this->method == "POST") {
            // Nếu là method POST -> thêm category mới.
            $data = json_decode(file_get_contents("php://input"));
            if (!isset($data->category_name) || $data->category_name == "") {
                $this->response(400, array("error" => "No category_name provided."));
            }
            $this->category->setCategory_name($data->category_name);
            $this->category->setCreated(date('Y-m-d H:i:s'));
            $result = $this->category->addCategory();
            if (isset($result["msg"])) {
                $this->response(200, ['msg' => $result["msg"]]);
            } else {
                $this->response(500, ['error' => $result["error"]]);
            }
        } else if ($this->method == "PUT") {
            // Nếu là method PUT -> Sửa thông tin của category
            $data = json_decode(file_get_contents("php://input"));
            $category_id = isset($data->category_id) ? $data->category_id : null;
            if ($category_id !== "") {
                // Nếu như có category_id !== "" -> Sửa thông tin của category có category_id tương ứng.
                if (!isset($data->category_name) || $data->category_name == "") {
                    $this->response(400, array("error" => "No category_name provided."));
                }
                $this->category->setCategory_id($category_id);
                $this->category->setCategory_name($data->category_name);
                $editCategory = $this->category->editCategory();
                if (isset($editCategory["msg"])) {
                    $this->response(200, $editCategory);
                } else {
                    $this->response(500, $editCategory);
                }
            } else if ($category_id === "") {
                $this->response(500, array("error" => "No Category ID provided."));
            }
        } else if ($this->method == "DELETE") {
            // Nếu là method DELETE -> xóa category có category_id tương ứng.
            $data = json_decode(file_get_contents("php://input"));
            $category_id = isset($data->category_id) ? $data->category_id : null;
            if ($category_id !== "") {
                // Nếu như có category_id !== "" -> Xóa category có category_id tương ứng.

                $this->category->setCategory_id($category_id);
                $del_category = $this->category->deleteCategory();
                if (isset($del_category["error"])) {
                    $this->response(500, $del_category);
                } else
                    $this->response(200, $del_category);
            } else if ($category_id === "") {
                $this->response(500, array("error" => "No Category ID provided."));
            }
        } else {
            $this->response(405, ["error" => "Method Not Allowed"]);
        }
    }
    function searchCategory()
    {
        if ($this->method == "GET") {
            $query = isset($_GET["q"]) ? $_GET["q"] : null;
            if ($query) {
                $this->category->setCategory_name($query);

                $search_arr = $this->category->searchCategory();
                if (isset($search_arr["error"])) {
                    $this->response(500, $search_arr);
                } else {
                    if (isset($search_arr["body"]))
                        $this->response(200, $search_arr);
                    $this->response(200, ["msg" => $search_arr]);
                }
            }
            if ($query === "") {
                $this->response(400, array("error" => "No query provided."));
            }
        } else {
            $this->response(405, ["error" => "Method Not Allowed"]);
        }
    }
}
