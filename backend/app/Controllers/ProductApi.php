<?php

/** ProductApi
 * 
 * 
 */

require_once 'app/Utils/RestfulAPI.php';
require_once 'app/Middlewares/Auth.php';
require_once 'app/config/Database.php';
require_once 'app/Models/Product.php';

class Product_API extends Restful_API
{

    private $database;
    private $db;
    private $product;

    function __construct()
    {
        $this->database = new Database();
        $this->db = $this->database->getConnection();
        $this->product = new Product($this->db);
        parent::__construct();
    }
    function product()
    {
        if ($this->method == "GET") {

            // Auth Middleware
            $role = [$_ENV["ROLE_NVQLK"], $_ENV["ROLE_ADMIN"]];
            $authMid = Auth::authMiddleware($role);
            if (isset($authMid["error"])) {
                $this->response(401, $authMid);
            }

            // Nếu là method GET -> lấy danh sách product
            $product_id = isset($_GET["product_id"]) ? $_GET["product_id"] : null;
            if ($product_id != "") {
                // Nếu như có category_id !== "" -> Lấy thông tin product.
                $this->product->setproduct_id($product_id);
                $product_arr = $this->product->getSingleProduct();
                if (isset($product_arr["body"])) {
                    $this->response(200, $product_arr);
                } else
                    $this->response(500, $product_arr);
            } else if ($product_id === "") {
                $this->response(500, array("error" => "No product ID provided."));
            } else {
                // Khi product_id = null -> Lấy toàn bộ danh sách thông tin product.
                $product_arr = $this->product->getAllProducts();
                if (isset($product_arr["body"])) {
                    $this->response(200, $product_arr);
                } else
                    $this->response(500, $product_arr);
            }
            //============================================================
        } else if ($this->method == "POST") {
            // Nếu là method POST -> thêm product mới.
            $data = json_decode(file_get_contents("php://input"));
            if (!isset($data->product_name) || $data->product_name == "") {
                $this->response(400, array("error" => "No product name provided."));
            }
            if (!isset($data->price) || $data->price == "" || $data->price == null) {
                $this->response(400, array("error" => "No Price provided."));
            }
            if (!isset($data->entry_price) || $data->entry_price == "" || $data->entry_price == null) {
                $this->response(400, array("error" => "No Entry Price provided."));
            }

            $this->product->setProduct_name($data->product_name);
            $this->product->setSku(isset($data->sku) ? $data->sku : null);
            $this->product->setCategory_id($data->category_id);
            $this->product->setDescription(isset($data->description) ? $data->description : null);
            $this->product->setPrice($data->price);
            $this->product->setPromotion_price(isset($data->promotion_price) && $data->promotion_price !== "" ? $data->promotion_price : null);
            $this->product->setEntry_price($data->entry_price);
            $this->product->setStock_quantity(isset($data->stock_quantity) && $data->stock_quantity !== "" ? $data->stock_quantity : null);
            $this->product->setCan_sale(isset($data->can_sale) && $data->can_sale !== "" ? $data->can_sale : null);
            $this->product->setLow_stock(isset($data->low_stock) && $data->low_stock !== "" ? $data->low_stock : null);
            $this->product->setTotal_sale(isset($data->total_sale) && $data->total_sale !== "" ? $data->total_total_sale : null);
            $this->product->setImage(isset($data->image) ? $data->image : null);
            $this->product->setCreated(date('Y-m-d H:i:s'));
            $result = $this->product->addProduct();

            if (isset($result["msg"])) {
                $this->response(200, ['msg' => $result["msg"]]);
            } else {
                $this->response(500, ['error' => $result["error"]]);
            }
            //============================================================
        } else if ($this->method == "PUT") {
            // Nếu là method PUT -> Sửa thông tin của category
            $data = json_decode(file_get_contents("php://input"));
            $product_id = isset($data->product_id) ? $data->product_id : null;
            if ($product_id !== "") {
                // Nếu như có product_id !== "" -> Sửa thông tin của category có product_id tương ứng.
                if (!isset($data->product_name) || $data->product_name == "") {
                    $this->response(400, array("error" => "No product name provided."));
                }
                if (!isset($data->price) || $data->price == "" || $data->price == null) {
                    $this->response(400, array("error" => "No Price provided."));
                }
                if (!isset($data->entry_price) || $data->entry_price == "" || $data->entry_price == null) {
                    $this->response(400, array("error" => "No Entry Price provided."));
                }
                $this->product->setProduct_id($data->product_id);
                $this->product->setProduct_name($data->product_name);
                $this->product->setSku(isset($data->sku) ? $data->sku : null);
                $this->product->setCategory_id($data->category_id);
                $this->product->setDescription(isset($data->description) ? $data->description : null);
                $this->product->setPrice($data->price);
                $this->product->setPromotion_price(isset($data->promotion_price) && $data->promotion_price !== "" ? $data->promotion_price : null);
                $this->product->setEntry_price($data->entry_price);
                $this->product->setStock_quantity(isset($data->stock_quantity) && $data->stock_quantity !== "" ? $data->stock_quantity : null);
                $this->product->setCan_sale(isset($data->can_sale) && $data->can_sale !== "" ? $data->can_sale : null);
                $this->product->setLow_stock(isset($data->low_stock) && $data->low_stock !== "" ? $data->low_stock : null);
                $this->product->setTotal_sale(isset($data->total_sale) && $data->total_sale !== "" ? $data->total_total_sale : null);
                $this->product->setImage(isset($data->image) ? $data->image : null);
                $editProduct = $this->product->editProduct();
                if (isset($editProduct["msg"])) {
                    $this->response(200, $editProduct);
                } else {
                    $this->response(500, $editProduct);
                }
            } else if ($product_id === "") {
                $this->response(500, array("error" => "No Category ID provided."));
            }
            //============================================================
        } else if ($this->method == "DELETE") {
            $data = json_decode(file_get_contents("php://input"));
            $product_id = isset($data->product_id) ? $data->product_id : null;
            if ($product_id !== "") {
                // Nếu như có product_id !== "" -> Xóa category có product_id tương ứng.

                $this->product->setProduct_id($product_id);
                $del_category = $this->product->deleteProduct();
                if (isset($del_category["error"])) {
                    $this->response(500, $del_category);
                } else
                    $this->response(200, $del_category);
            } else if ($product_id === "") {
                $this->response(500, array("error" => "No Category ID provided."));
            }
        } else {
            $this->response(405, ["error" => "Method Not Allowed"]);
        }
    }
}
