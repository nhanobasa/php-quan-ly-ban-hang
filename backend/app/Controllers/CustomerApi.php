<?php
require_once 'app/Utils/RestfulAPI.php';
require_once 'app/Middlewares/Auth.php';
require_once 'app/config/Database.php';
require_once 'app/Models/Customer.php';

class Customer_API extends Restful_API
{

    private $database;
    private $db;
    private $customer;

    function __construct()
    {
        $this->database = new Database();
        $this->db = $this->database->getConnection();
        $this->customer = new Customer($this->db);
        parent::__construct();
    }
    function addCustomer()
    {
        if ($this->method == "POST") {
            // Auth Middleware
            $role = [$_ENV["ROLE_NVBH"]];
            $authMid = Auth::authMiddleware($role);
            if (isset($authMid["error"])) {
                $this->response(401, $authMid);
            }

            $data = $_POST;
            if (!isset($data["name"]) || $data["name"] == "")
                $this->response(400, array("error" => "No name provided."));

            else if (!isset($data["email"]) || $data["email"] == "")
                $this->response(400, array("error" => "No email provided."));

            else if (!isset($data["phone"]) || $data["phone"] == "")
                $this->response(400, array("error" => "No phone number provided."));

            $this->customer->setName($data["name"]);
            $this->customer->setPhone($data["phone"]);
            $this->customer->setEmail($data["email"]);
            $this->customer->setAddress($data["address"]);
            $this->customer->setDistrict($data["district"]);
            $this->customer->setCity($data["city"]);
            $this->customer->setSex($data["sex"]);
            $this->customer->setDate(date("Y-m-d", strtotime($data["date"])));
            $this->customer->setCreated(date('Y-m-d H:i:s'));

            $result = $this->customer->addCustomer();
            if (isset($result["msg"])) {
                $this->response(200, ['msg' => $result["msg"]]);
            } else {
                $this->response(500, ['error' => $result["error"]]);
            }
        } else
            $this->response(405, ["error" => "Method Not Allowed"]);
    }

    function customer()
    {
        if ($this->method == "GET") {
            // Auth Middleware
            $role = [$_ENV["ROLE_NVBH"], $_ENV["ROLE_ADMIN"]];
            $authMid = Auth::authMiddleware($role);
            if (isset($authMid["error"])) {
                $this->response(401, $authMid);
            }
            $customerID = isset($_GET["id"]) ? $_GET["id"] : null;
            if ($customerID) {
                $this->customer->setId($customerID);
                $customer_arr = $this->customer->getSingleCustomer();
                if (isset($customer_arr["body"])) {
                    $this->response(200, $customer_arr);
                } else
                    $this->response(500, $customer_arr);
            }
            if ($customerID === "") {
                $this->response(500, array("error" => "No ID provided."));
            }
            $customer_arr = $this->customer->getAllCustomers();

            if (isset($customer_arr["body"])) {
                $this->response(200, $customer_arr);
            } else
                $this->response(500, $customer_arr);
        } else
            $this->response(405, ["error" => "Method Not Allowed"]);
    }

    function editcustomer()
    {
        if ($this->method == "PUT") {
            // Auth Middleware
            $role = [$_ENV["ROLE_NVBH"], $_ENV["ROLE_ADMIN"]];
            $authMid = Auth::authMiddleware($role);
            if (isset($authMid["error"])) {
                $this->response(401, $authMid);
            }
            $data = json_decode(file_get_contents("php://input"));
            $customerID = isset($data->id) ? $data->id : null;
            if ($customerID) {
                if (!isset($data->name))
                    $this->response(400, array("error" => "No name provided."));

                else if (!isset($data->email))
                    $this->response(400, array("error" => "No email provided."));

                else if (!isset($data->phone))
                    $this->response(400, array("error" => "No phone number provided."));

                $this->customer->setId($customerID);
                $this->customer->setName($data->name);
                $this->customer->setPhone($data->phone);
                $this->customer->setEmail($data->email);
                $this->customer->setAddress($data->address);
                $this->customer->setDistrict($data->district);
                $this->customer->setCity($data->city);
                $this->customer->setSex($data->sex);
                $this->customer->setDate(date("Y-m-d", strtotime($data->date)));
                $editCustomer = $this->customer->editCustomer();
                if (isset($editCustomer["msg"])) {
                    $this->response(200, $editCustomer);
                } else {
                    $this->response(500, $editCustomer);
                }
            }
            if ($customerID === "") {
                $this->response(500, array("error" => "No ID provided."));
            }
        } else
            $this->response(405, ["error" => "Method Not Allowed"]);
    }

    function delCustomer()
    {
        if ($this->method == "DELETE") {
            // Auth Middleware
            $role = [$_ENV["ROLE_NVBH"], $_ENV["ROLE_ADMIN"]];
            $authMid = Auth::authMiddleware($role);
            if (isset($authMid["error"])) {
                $this->response(401, $authMid);
            }

            $data = json_decode(file_get_contents("php://input"));
            $customerID = isset($data->id) ? $data->id : null;
            if ($customerID) {
                $this->customer->setId($customerID);
                $del_customer = $this->customer->delCustomer();
                if (isset($del_customer["error"])) {
                    $this->response(500, $del_customer);
                } else
                    $this->response(200, $del_customer);
            }
            if ($customerID === "") {
                $this->response(400, array("error" => "No ID provided."));
            }
        } else
            $this->response(405, ["error" => "Method Not Allowed"]);
    }

    function searchCustomer()
    {
        if ($this->method == "GET") {
            // Auth Middleware
            $role = [$_ENV["ROLE_NVBH"], $_ENV["ROLE_ADMIN"]];
            $authMid = Auth::authMiddleware($role);
            if (isset($authMid["error"])) {
                $this->response(401, $authMid);
            }
            $query = isset($_GET["q"]) ? $_GET["q"] : null;
            if ($query) {
                $this->customer->setName($query);
                $this->customer->setPhone($query);
                $search_arr = $this->customer->searchCustomer();
                if (isset($search_arr["body"])) {
                    $this->response(200, $search_arr);
                } else
                    $this->response(500, $search_arr);
            }
            if ($query == "") {
                $this->response(400, array("error" => "No query provided."));
            }
        } else
            $this->response(405, ["error" => "Method Not Allowed"]);
    }
}
