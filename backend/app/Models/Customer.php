<?php

class Customer
{

    private $conn;
    private $id;
    private $name;
    private $email;
    private $phone;
    private $address;
    private $district;
    private $city;
    private $sex;
    private $date;
    private $created;

    function __construct($db)
    {
        $this->conn = $db;
    }

    function setId($id): void
    {
        $this->id = $id;
    }

    function setName($name): void
    {
        $this->name = $name;
    }

    function setEmail($email): void
    {
        $this->email = $email;
    }

    function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    function setAddress($address): void
    {
        $this->address = $address;
    }

    function setDistrict($district): void
    {
        $this->district = $district;
    }

    function setCity($city): void
    {
        $this->city = $city;
    }

    function setSex($sex): void
    {
        $this->sex = $sex;
    }

    function setDate($date): void
    {
        $this->date = $date;
    }

    function setCreated($created): void
    {
        $this->created = $created;
    }

    function getId()
    {
        return $this->id;
    }

    function getName()
    {
        return $this->name;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getPhone()
    {
        return $this->phone;
    }

    function getAddress()
    {
        return $this->address;
    }

    function getDistrict()
    {
        return $this->district;
    }

    function getCity()
    {
        return $this->city;
    }

    function getSex()
    {
        return $this->sex;
    }

    function getDate()
    {
        return $this->date;
    }

    function getCreated()
    {
        return $this->created;
    }

    function addCustomer()
    {
        $sqlQuery = "INSERT INTO Customers (name, email, phone, address, district, city, sex, date, created) "
            . "VALUES (:name, :email, :phone, :address, :district, :city, :sex, :date, :created)";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            //        Data binding
            $stmt->bindValue(":name", $this->testData($this->name));
            $stmt->bindValue(":email", $this->testData($this->email));
            $stmt->bindValue(":phone", $this->testData($this->phone));
            $stmt->bindValue(":address", $this->testData($this->address));
            $stmt->bindValue(":district", $this->testData($this->district));
            $stmt->bindValue(":city", $this->testData($this->city));
            $stmt->bindValue(":sex", $this->testData($this->sex));
            $stmt->bindValue(":date", $this->testData($this->date));
            $stmt->bindValue(":created", $this->testData($this->created));
            $stmt->execute();
            return array("msg" => "Create customer successful.");
        } catch (PDOException $e) {
            if ($e->errorInfo[1] == 1062) {
                $arr_err = explode("1062", $e->getMessage());
                return array("error" => $arr_err[1]);
            } else {
                return array("error" => $e->getMessage());
            }
        }
    }

    function editCustomer()
    {
        $sql_check_id_cate_exist = "SELECT id FROM Customers WHERE id REGEXP '[[:<:]]"
            . $this->testData($this->id) . "[[:>:]]'";
        $sqlQuery = "UPDATE Customers SET name = :name, email = :email, phone = :phone, address = :address, district = :district, city = :city,"
            . "sex = :sex, date = :date WHERE id = :id";
        try {
            $stmt = $this->conn->prepare($sql_check_id_cate_exist);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount) {
                $stmt = $this->conn->prepare($sqlQuery);
                //        Data binding
                $stmt->bindValue(":id", $this->testData($this->id));
                $stmt->bindValue(":name", $this->testData($this->name));
                $stmt->bindValue(":email", $this->testData($this->email));
                $stmt->bindValue(":phone", $this->testData($this->phone));
                $stmt->bindValue(":address", $this->testData($this->address));
                $stmt->bindValue(":district", $this->testData($this->district));
                $stmt->bindValue(":city", $this->testData($this->city));
                $stmt->bindValue(":sex", $this->testData($this->sex));
                $stmt->bindValue(":date", $this->testData($this->date));
                $stmt->execute();
                return array("msg" => "Edit customer successful.");
            } else {
                return array("msg" => "No customer edited");
            }
        } catch (PDOException $exc) {
            if ($exc->errorInfo[1] == 1062) {
                $arr_err = explode("1062", $exc->getMessage());
                return array("error" => $arr_err[1]);
            } else {
                return array("error" => $exc->getMessage());
            }
        }
    }
    // Get all Customer
    function getAllCustomers()
    {
        $sqlQuery = "SELECT *, DATE_FORMAT(date, \"%d-%m-%Y\") as date FROM Customers";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount > 0) {
                $customer_arr = array();
                $customer_arr["body"] = array();
                $customer_arr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "id" => $id,
                        "name" => $name,
                        "email" => $email,
                        "phone" => $phone,
                        "address" => $address,
                        "district" => $district,
                        "city" => $city,
                        "sex" => $sex,
                        "date" => $date,
                        "created" => $created
                    );
                    array_push($customer_arr["body"], $e);
                }
                return $customer_arr;
            } else {
                return array("Msg" => "No user.");
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }

    // Get single Customer
    function getSingleCustomer()
    {
        $sqlQuery = "SELECT *, DATE_FORMAT(date, \"%d-%m-%Y\") as date FROM Customers WHERE id REGEXP '[[:<:]]"
            . $this->testData($this->id) . "[[:>:]]'";
        try {
            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $customer_arr = array();
                $customer_arr["body"] = array();
                $customer_arr['itemCount'] = $itemCount;

                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                extract($row);
                $e = array(
                    "id" => $id,
                    "name" => $name,
                    "email" => $email,
                    "phone" => $phone,
                    "address" => $address,
                    "district" => $district,
                    "city" => $city,
                    "sex" => $sex,
                    "date" => $date,
                    "created" => $created
                );
                array_push($customer_arr["body"], $e);

                return $customer_arr;
            } else {
                return "Msg: No user.";
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }
    // Delete Customer
    function delCustomer()
    {
        if (is_array($this->id)) {
            $msg = array();
            foreach ($this->id as $key => $id) {
                $sql_check_id_cate_exist = "SELECT id FROM Customers WHERE id REGEXP '[[:<:]]"
                    . $this->testData($id) . "[[:>:]]'";
                $sqlQuery = "DELETE FROM Customers WHERE id REGEXP '[[:<:]]"
                    . $this->testData($id) . "[[:>:]]'";
                try {
                    $stmt = $this->conn->prepare($sql_check_id_cate_exist);
                    $stmt->execute();

                    $itemCount = $stmt->rowCount();
                    if ($itemCount) {
                        $stmt = $this->conn->prepare($sqlQuery);
                        $stmt->execute();
                        array_push($msg, "Customer with id = " . $id . " deleted successfully");
                    }
                } catch (PDOException $exc) {
                    return array("error" => $exc->getMessage());
                }
            }
            if (count($msg)) {
                return array("msg" => $msg);
            } else {
                return array("msg" => "No customer deleted");
            }
        }
    }

    // Search Customer
    function searchCustomer()
    {
        $sqlQuery = "SELECT *, DATE_FORMAT(date, \"%d-%m-%Y\") as date FROM Customers WHERE name LIKE '%" . $this->testData($this->name) . "%' OR phone LIKE '%" . $this->testData($this->phone) . "%'";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $customer_arr = array();
                $customer_arr["body"] = array();
                $customer_arr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "id" => $id,
                        "name" => $name,
                        "email" => $email,
                        "phone" => $phone,
                        "address" => $address,
                        "district" => $district,
                        "city" => $city,
                        "sex" => $sex,
                        "date" => $date,
                        "created" => $created
                    );
                    array_push($customer_arr["body"], $e);
                }

                return $customer_arr;
            } else {
                return "Msg: No user.";
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }
    private function testData($data)
    {
        return htmlspecialchars(strip_tags(trim($data)));
    }
}
