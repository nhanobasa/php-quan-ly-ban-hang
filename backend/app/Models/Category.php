<?php
//**
//* Category Model
//*/

class Category
{
    // Connection
    private $db;

    // Colums
    private $category_id;
    private $category_name;

    private $created;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getCategory_id()
    {
        return $this->category_id;
    }

    function getCategory_name()
    {
        return $this->category_name;
    }


    function getCreated()
    {
        return $this->created;
    }

    function setCategory_id($category_id): void
    {
        $this->category_id = $category_id;
    }

    function setCategory_name($category_name): void
    {
        $this->category_name = $category_name;
    }

    function setCreated($created): void
    {
        $this->created = $created;
    }
    // Add new category
    function addCategory()
    {
        $sqlQuery = "INSERT INTO category (category_name, created)"
            . "VALUES (:category_name, :created)";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            // Data binding
            $stmt->bindValue(":category_name", $this->testData($this->category_name));
            $stmt->bindValue(":created", $this->testData($this->created));
            $stmt->execute();
            return array("msg" => "Create customer successful.");
        } catch (PDOException $exc) {
            if ($exc->errorInfo[1] == 1062) {
                return array("error" => "Tên danh mục đã tồn tại!");
            } else if ($exc->errorInfo[1] == 1452) {
                return array("error" => "Danh mục cha không tồn tại");
            } else {
                return array("error" => $exc->getMessage());
            }
        }
    }

    // Get all Customer
    function getAllCategories()
    {
        $sqlQuery = "SELECT category.*, COUNT(product.product_id) AS total_product FROM category"
            . " LEFT JOIN product ON category.category_id = product.category_id GROUP BY(category_id)";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount > 0) {
                $category_arr = array();
                $category_arr["body"] = array();
                $category_arr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    array_push($category_arr["body"], $row);
                }
                return $category_arr;
            } else {
                return array("Msg" => "No user.");
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }

    function getProductsOfCategory()
    {
        $sqlQueryCategory = "SELECT * FROM category WHERE category_id REGEXP '[[:<:]]"
            . $this->testData($this->category_id) . "[[:>:]]'";
        $sqlQueryProductCategory = "SELECT * FROM product WHERE category_id REGEXP '[[:<:]]"
            . $this->testData($this->category_id) . "[[:>:]]'";
        try {
            $stmt = $this->conn->prepare($sqlQueryCategory);

            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $category_arr = array();
                $category_arr["body"] = array();
                $category_arr['itemCount'] = $itemCount;

                $rowCategory = $stmt->fetch(PDO::FETCH_ASSOC);
                extract($rowCategory);
                $e = array(
                    "category_id" => $category_id,
                    "category_name" => $category_name,
                    "created" => $created
                );
                $e["product_list"] = array();

                $stmt = $this->conn->prepare($sqlQueryProductCategory);
                $stmt->execute();
                $product = array();
                while ($rowProductCategory = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    array_push($e["product_list"], $rowProductCategory);
                }

                array_push($category_arr["body"], $e);
                return $category_arr;
            } else {
                return "Msg: Category not found.";
            }
        } catch (PDOException  $exc) {
            return array("error" => $exc->getMessage());
        }
    }

    function editCategory()
    {
        $sql_check_id_cate_exist = "SELECT category_id FROM category WHERE category_id REGEXP '[[:<:]]"
            . $this->testData($this->category_id) . "[[:>:]]'";
        $sqlQuery = "UPDATE category SET category_name = :category_name WHERE category_id = :category_id";
        try {
            $stmt = $this->conn->prepare($sql_check_id_cate_exist);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount) {
                $stmt = $this->conn->prepare($sqlQuery);
                //        Data binding
                $stmt->bindValue(":category_id", $this->testData($this->category_id));
                $stmt->bindValue(":category_name", $this->testData($this->category_name));

                $stmt->execute();
                return array("msg" => "Edit category successful.");
            } else {
                return array("msg" => "No category edited");
            }
        } catch (PDOException $exc) {
            if ($exc->errorInfo[1] == 1062) {
                return array("error" => "Tên danh mục đã tồn tại!");
            } else if ($exc->errorInfo[1] == 1452) {
                return array("error" => "Danh mục cha không tồn tại");
            } else {
                return array("error" => $exc->getMessage());
            }
        }
    }

    // DELETE category
    function deleteCategory()
    {
        if (is_array($this->category_id)) {
            $msg = array();
            foreach ($this->category_id as $key => $id) {
                $sql_check_id_cate_exist = "SELECT category_id FROM category WHERE category_id REGEXP '[[:<:]]"
                    . $this->testData($id) . "[[:>:]]'";
                $sqlQuery = "DELETE FROM category WHERE category_id REGEXP '[[:<:]]"
                    . $this->testData($id) . "[[:>:]]'";
                try {
                    $stmt = $this->conn->prepare($sql_check_id_cate_exist);
                    $stmt->execute();

                    $itemCount = $stmt->rowCount();
                    if ($itemCount) {
                        $stmt = $this->conn->prepare($sqlQuery);
                        $stmt->execute();
                        $sqlQuery = "UPDATE product SET category_id = 0 WHERE category_id is NULL";
                        $stmt = $this->conn->prepare($sqlQuery);
                        $test = $stmt->execute();
                        array_push($msg, "Category with id = " . $id . " deleted successfully");
                    }
                } catch (PDOException $exc) {
                    return array("error" => $exc->getMessage());
                }
            }
            if (count($msg)) {
                return array("msg" => $msg);
            } else {
                return array("msg" => "No category deleted");
            }
        }
    }
    // Search Category
    function searchCategory()
    {
        $sqlQuery = "SELECT * FROM category WHERE category_name LIKE '%" . $this->testData($this->category_name) . "%'";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $customer_arr = array();
                $customer_arr["body"] = array();
                $customer_arr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

                    array_push($customer_arr["body"], $row);
                }

                return $customer_arr;
            } else {
                return "Msg: No Category.";
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }
    private function testData($data)
    {
        if ($data !== null) {
            return htmlspecialchars(strip_tags(trim($data)));
        }
        return null;
    }
}
