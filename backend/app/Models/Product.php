<?php
//** Product Model*/
require_once('app/Utils/Utils.php');

class Product
{
    // Connection
    private $conn;
    // Columns
    private $product_id;
    private $product_name;
    private $sku;
    private $category_id;
    private $description;
    private $price; // giá bán thường
    private $promotion_price; // giá bán khuyến mãi
    private $entry_price; // giá nhập vào
    private $stock_quantity; // số lượng tồn kho
    private $can_sale; // số lượng có thể bán
    private $low_stock; // ngưỡng sản phẩm thấp
    private $total_sale; // tông số đã bán
    private $image; // đường dẫn ảnh sản phẩm
    private $created; // thời gian tạo

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getProduct_id()
    {
        return $this->product_id;
    }

    function getProduct_name()
    {
        return $this->product_name;
    }

    function getSku()
    {
        return $this->sku;
    }

    function getCategory_id()
    {
        return $this->category_id;
    }

    function getDescription()
    {
        return $this->description;
    }

    function getPrice()
    {
        return $this->price;
    }

    function getPromotion_price()
    {
        return $this->promotion_price;
    }

    function getEntry_price()
    {
        return $this->entry_price;
    }

    function getStock_quantity()
    {
        return $this->stock_quantity;
    }

    function getCan_sale()
    {
        return $this->can_sale;
    }

    function getLow_stock()
    {
        return $this->low_stock;
    }

    function getTotal_sale()
    {
        return $this->total_sale;
    }

    function getImage()
    {
        return $this->image;
    }

    function setProduct_id($product_id): void
    {
        $this->product_id = $product_id;
    }

    function setProduct_name($product_name): void
    {
        $this->product_name = $product_name;
    }

    function setSku($sku): void
    {
        $this->sku = $sku;
    }

    function setCategory_id($category_id): void
    {
        $this->category_id = $category_id;
    }

    function setDescription($description): void
    {
        $this->description = $description;
    }

    function setPrice($price): void
    {
        $this->price = $price;
    }

    function setPromotion_price($promotion_price): void
    {
        $this->promotion_price = $promotion_price;
    }

    function setEntry_price($entry_price): void
    {
        $this->entry_price = $entry_price;
    }

    function setStock_quantity($stock_quantity): void
    {
        $this->stock_quantity = $stock_quantity;
    }

    function setCan_sale($can_sale): void
    {
        $this->can_sale = $can_sale;
    }

    function setLow_stock($low_stock): void
    {
        $this->low_stock = $low_stock;
    }

    function setTotal_sale($total_sale): void
    {
        $this->total_sale = $total_sale;
    }

    function setImage($image): void
    {
        $this->image = $image;
    }

    function getCreated()
    {
        return $this->created;
    }

    function setCreated($created): void
    {
        $this->created = $created;
    }
    // Add new Product
    function addProduct()
    {
        $sqlQuery = "INSERT INTO product(product_name, sku, category_id, description, price, promotion_price, entry_price, stock_quantity, can_sale, low_stock, total_sale, image, created) "
            . "VALUES (:product_name, :sku, :category_id, :description, :price, :promotion_price, :entry_price, :stock_quantity, :can_sale, :low_stock, :total_sale, :image, :created)";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            // Data binding
            // $stmt->bindValue(":product_id", $this->testData($this->product_id));
            $this->sku = $this->testData($this->sku) != null && $this->testData($this->sku) != "" ? $this->testData($this->sku) : $this->genProSKU();
            $stmt->bindValue(":product_name", $this->testData($this->product_name));
            $stmt->bindValue(":sku", $this->sku);
            $stmt->bindValue(":category_id", $this->testData($this->category_id));
            $stmt->bindValue(":description", $this->testData($this->description));
            $stmt->bindValue(":price", $this->testData($this->price));
            $stmt->bindValue(":promotion_price", $this->testData($this->promotion_price));
            $stmt->bindValue(":entry_price", $this->testData($this->entry_price));
            $stmt->bindValue(":stock_quantity", $this->testData($this->stock_quantity));
            $stmt->bindValue(":can_sale", $this->testData($this->can_sale));
            $stmt->bindValue(":low_stock", $this->testData($this->low_stock));
            $stmt->bindValue(":total_sale", $this->testData($this->total_sale));
            $stmt->bindValue(":image", $this->saveImage($this->sku, $this->image));
            $stmt->bindValue(":created", $this->testData($this->created));
            $stmt->execute();
            return array("msg" => "Tạo sản phẩm thành công.");
        } catch (PDOException $exc) {
            if ($exc->errorInfo[1] == 1062) {
                return array("error" => "Mã SKU đã tồn tại!");
            }
            return array("error" => $exc->getMessage());
        }
    }
    // Get all product
    function getAllProducts()
    {
        $sqlQuery = "SELECT product.*, category_name FROM product LEFT JOIN category ON product.category_id = category.category_id";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount > 0) {
                $product_arr = array();
                $product_arr["body"] = array();
                $product_arr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    array_push($product_arr["body"], $row);
                }
                return $product_arr;
            } else {
                return array("Msg" => "No product.");
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }
    // Get all product
    function getSingleProduct()
    {
        $sqlQuery = "SELECT product.*, category_name FROM product LEFT JOIN category ON product.category_id = category.category_id WHERE product_id REGEXP '[[:<:]]"
            . $this->testData($this->product_id) . "[[:>:]]'";
        try {
            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $product_arr = array();
                $product_arr["body"] = array();
                $product_arr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    array_push($product_arr["body"], $row);
                }
                return $product_arr;
            } else {
                return array("Msg" => "No product.");
            }
        } catch (PDOException $exc) {

            return array("error" => $exc->getMessage());
        }
    }
    // Edit product
    function editProduct()
    {
        $sql_check_id_cate_exist = "SELECT product_id FROM product WHERE product_id REGEXP '[[:<:]]"
            . $this->testData($this->product_id) . "[[:>:]]'";
        $sqlQuery = "UPDATE product SET product_name = :product_name, sku = :sku, category_id = :category_id,"
            . " description = :description, price = :price, promotion_price = :promotion_price,"
            . " entry_price = :entry_price, stock_quantity= :stock_quantity, can_sale = :can_sale,"
            . " low_stock = :low_stock, total_sale = :total_sale, image = :image WHERE product_id = :product_id";
        try {
            $stmt = $this->conn->prepare($sql_check_id_cate_exist);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount) {
                $stmt = $this->conn->prepare($sqlQuery);
                //        Data binding
                $this->sku = $this->testData($this->sku) != null && $this->testData($this->sku) != "" ? $this->testData($this->sku) : $this->genProSKU();
                $stmt->bindValue(":product_id", $this->testData($this->product_id));
                $stmt->bindValue(":product_name", $this->testData($this->product_name));
                $stmt->bindValue(":sku", $this->sku);
                $stmt->bindValue(":category_id", $this->testData($this->category_id));
                $stmt->bindValue(":description", $this->testData($this->description));
                $stmt->bindValue(":price", $this->testData($this->price));
                $stmt->bindValue(":promotion_price", $this->testData($this->promotion_price));
                $stmt->bindValue(":entry_price", $this->testData($this->entry_price));
                $stmt->bindValue(":stock_quantity", $this->testData($this->stock_quantity));
                $stmt->bindValue(":can_sale", $this->testData($this->can_sale));
                $stmt->bindValue(":low_stock", $this->testData($this->low_stock));
                $stmt->bindValue(":total_sale", $this->testData($this->total_sale));
                $stmt->bindValue(":image", $this->saveImage($this->sku, $this->image));

                $stmt->execute();
                return array("msg" => "Edit product successful.");
            } else {
                return array("msg" => "No product edited");
            }
        } catch (PDOException $exc) {
            if ($exc->errorInfo[1] == 1062) {
                return array("error" => "Tên danh mục đã tồn tại!");
            } else if ($exc->errorInfo[1] == 1452) {
                return array("error" => "Danh mục cha không tồn tại");
            } else {
                return array("error" => $exc->getMessage());
            }
        }
    }
    // DELETE product
    function deleteProduct()
    {
        if (is_array($this->product_id)) {
            $msg = array();
            foreach ($this->product_id as $key => $id) {
                $sql_check_id_cate_exist = "SELECT product_id FROM product WHERE product_id REGEXP '[[:<:]]"
                    . $this->testData($id) . "[[:>:]]'";
                $sqlQuery = "DELETE FROM product WHERE product_id REGEXP '[[:<:]]"
                    . $this->testData($id) . "[[:>:]]'";
                try {
                    $stmt = $this->conn->prepare($sql_check_id_cate_exist);
                    $stmt->execute();

                    $itemCount = $stmt->rowCount();
                    if ($itemCount) {
                        $stmt = $this->conn->prepare($sqlQuery);
                        $stmt->execute();
                        $sqlQuery = "UPDATE product SET product_id = 0 WHERE product_id is NULL";
                        $stmt = $this->conn->prepare($sqlQuery);
                        $test = $stmt->execute();
                        array_push($msg, "Category with id = " . $id . " deleted successfully");
                    }
                } catch (PDOException $exc) {
                    return array("error" => $exc->getMessage());
                }
            }
            if (count($msg)) {
                return array("msg" => $msg);
            } else {
                return array("msg" => "No category deleted");
            }
        }
    }
    // Tiền xử lý dữ liệu đầu vào.
    private function testData($data)
    {
        if ($data !== null) {
            return htmlspecialchars(strip_tags(trim($data)));
        }
        return null;
    }
    // Sinh mã SKU ngẫu nhiên nếu như người dùng không nhập vào.
    private function genProSKU()
    {
        try {
            if ($this->testData($this->category_id)) {
                $sqlQuery = "SELECT COUNT(*) AS itemCount FROM product WHERE category_id REGEXP '[[:<:]]"
                    . $this->testData($this->category_id) . "[[:>:]]'";
                $stmt = $this->conn->prepare($sqlQuery);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $itemCount = $row["itemCount"];

                $sqlQueryCategory = "SELECT category_name FROM category WHERE category_id REGEXP '[[:<:]]"
                    . $this->testData($this->category_id) . "[[:>:]]'";
                $stmt = $this->conn->prepare($sqlQueryCategory);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $preSKU = '';
                $category_name_arr = explode(" ", Utils::vn_to_str($this->testData($row["category_name"])));
                foreach ($category_name_arr as $element) {
                    $preSKU = $preSKU . $element[0];
                }
                $nextSKU = strtoupper($preSKU) . (string) ($itemCount + 1);
                return $nextSKU;
            } else {
                $sqlQuery = "SELECT product.sku"
                    . " FROM product, (SELECT MAX(LENGTH(sku)) AS ml FROM product WHERE sku LIKE '%SKU%') AS A"
                    . " WHERE LENGTH(product.sku) = A.ml";
                $stmt = $this->conn->prepare($sqlQuery);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $itemCount = explode("SKU", $row["sku"])[1];
                $nextSKU = "SKU" . (string) ($itemCount + 1);
                return $nextSKU;
            }
        } catch (PDOException $exc) {
            return array("error" => $exc->getMessage());
        }
    }
    private function saveImage($product_id, $base64String)
    {
        // Obtain the original content (usually binary data)
        if (!isset($base64String) || $base64String == "") {
            return "";
        }
        $bs64 = explode("base64,", $base64String);
        $bin = base64_decode($bs64[1]);

        // Load GD resource from binary data
        $im = imageCreateFromString($bin);
        // Make sure that the GD library was able to load the image
        // This is important, because you should not miss corrupted or unsupported images
        if (!$im) {
            return "";
        }
        // Specify the location where you want to save the image
        $fileName = "app/images/" . $product_id . ".png";
        // $pro_imageFile = fopen($fileName, "w") or die("Fail to create File");
        // fwrite($pro_imageFile, $base64String);
        // fclose($pro_imageFile);
        imagepng($im, $fileName, 0);

        return $fileName;
    }
}
