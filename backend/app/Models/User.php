<?php

require_once 'vendor/autoload.php';

use Firebase\JWT\JWT;

class User
{

    // Connection
    private $conn;
    // Columns
    private $id;
    private $password;
    private $name;
    private $phone;
    private $email;
    private $date;
    private $role;
    private $address;
    private $created;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function setID($id): void
    {
        $this->id = $id;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function setRole($role): void
    {
        $this->role = $role;
    }

    public function setAddress($address): void
    {
        $this->address = $address;
    }

    public function setCreated($created): void
    {
        $this->created = $created;
    }

    function getId()
    {
        return $this->id;
    }

    function getPassword()
    {
        return $this->password;
    }

    function getName()
    {
        return $this->name;
    }

    function getPhone()
    {
        return $this->phone;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getDate()
    {
        return $this->date;
    }

    function getRole()
    {
        return $this->role;
    }

    function getAddress()
    {
        return $this->address;
    }

    function getCreated()
    {
        return $this->created;
    }

    // Get all users
    function getUsers()
    {
        $sqlQuery = "SELECT *, DATE_FORMAT(date, \"%d-%m-%Y\") as date FROM User";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            $itemCount = $stmt->rowCount();

            if ($itemCount > 0) {
                $userArr = array();
                $userArr["body"] = array();
                $userArr['itemCount'] = $itemCount;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $e = array(
                        "id" => $id,
                        "name" => $name,
                        "email" => $email,
                        "phone" => $phone,
                        "date" => $date,
                        "role" => $role,
                        "address" => $address,
                        "created" => $created
                    );
                    array_push($userArr["body"], $e);
                }
                return $userArr;
            } else {
                return array("Msg" => "No user.");
            }
        } catch (PDOException $exc) {
            return array("msg" => array("error" => $exc->getMessage()), "status" => 500);
        }
        //        
    }

    // Get single User
    function getSingleUser()
    {
        $sqlQuery = "SELECT *, DATE_FORMAT(date, \"%d-%m-%Y\") as date FROM User WHERE id REGEXP '[[:<:]]"
            . $this->testData($this->id) . "[[:>:]]'";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindValue(":id", $this->testData($this->id));
            $stmt->execute();

            $itemCount = $stmt->rowCount();
            if ($itemCount > 0) {
                $userArr = array();
                $userArr["body"] = array();
                $userArr['itemCount'] = $itemCount;

                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                extract($row);
                $e = array(
                    "id" => $id,
                    "name" => $name,
                    "email" => $email,
                    "phone" => $phone,
                    "date" => $date,
                    "role" => $role,
                    "address" => $address,
                    "created" => $created
                );
                array_push($userArr["body"], $e);

                return $userArr;
            } else {
                return "Msg: No user.";
            }
        } catch (PDOException $exc) {
            return array("msg" => array("error" => $exc->getMessage()), "status" => 500);
        }
    }

    function createUser()
    {
        $sqlQuery = "INSERT INTO User (name, phone, email, password, date, role, address, created) "
            . "VALUES (:name, :phone, :email, :password, :date, :role, :address, :created)";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            //        Data binding
            $stmt->bindValue(":name", $this->testData($this->name));
            $stmt->bindValue(":email", $this->testData($this->email));
            $stmt->bindValue(":phone", $this->testData($this->phone));

            $password_hashed = password_hash($this->testData($this->password), PASSWORD_BCRYPT, ["cost" => 11]);
            $stmt->bindValue(":password", $password_hashed);

            $stmt->bindValue(":date", $this->testData($this->date));
            $stmt->bindValue(":role", $this->testData($this->role));
            $stmt->bindValue(":address", $this->testData($this->address));
            $stmt->bindValue(":created", $this->testData($this->created));
            $stmt->execute();
            return array("msg" => array("msg" => "Create user successful."), "status" => 200);
        } catch (PDOException $e) {
            if ($e->errorInfo[1] == 1062) {
                $arr_err = explode("1062", $e->getMessage());
                return array("msg" => array("error" => $arr_err[1]), "status" => 500);
            } else {
                return array("msg" => array("error" => $e->getMessage()), "status" => 500);
            }
        }
    }

    function findByCredentials()
    {
        $sqlQuery = "SELECT * FROM User WHERE email = :email";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            //        Data binding
            $stmt->bindValue(":email", $this->testData($this->email));

            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!password_verify($this->testData($this->password), $row["password"])) {
                throw new Exception("Invalid login credentials");
            }
            $profile = [
                "userID" => $row["id"],
                "name" => $row["name"],
                "email" => $row["email"],
                "role" => $row["role"]
            ];
            $accesstoken = $this->generateAccessToken($profile, $_ENV["ACCESSTOKENKEY"], $_ENV['TOKEN_LIFE']); // TOKEN_LIFE seconds
            return array("access_token" => $accesstoken, "profile" => $profile);
        } catch (PDOException $exc) {
            throw new Exception($exc->getMessage);
        }
    }

    private function generateAccessToken($data, $key, $expTime)
    {
        $tokenId = uniqid(rand(), true);
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + $expTime;            // Adding expTime seconds
        $serverName = $_SERVER["SERVER_NAME"]; // Retrieve the server name from config file

        /*
         * Create the token as an array
         */

        $payload = [
            'iat' => $issuedAt, // Issued at: time when the token was generated
            'jti' => $tokenId, // Json Token Id: an unique identifier for the token
            'iss' => $serverName, // Issuer
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => $data
        ];

        $accessToken = JWT::encode($payload, $key);
        return $accessToken;
    }

    private function testData($data)
    {
        return htmlspecialchars(strip_tags(trim($data)));
    }
}
