let profile = JSON.parse(localStorage.getItem('profile'));

$('.profile_title').text(`Chào mừng ${profile.name} quay trở lại!`);

let role = profile.role;
if (role == 'ADMIN') {
    $('.user_role').text('Chủ shop');
} else if (role == 'NVBH') {
    $('.user_role').text('Nhân viên bán hàng');
} else if (role == 'NVQLK') {
    $('.user_role').text('Nhân viên quản lý kho');
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhonenumber(inputtxt) {
    var phoneno = /([0-9]{10,11})/;
    if (inputtxt.match(phoneno)) {
        return true;
    } else {
        return false;
    }
}
