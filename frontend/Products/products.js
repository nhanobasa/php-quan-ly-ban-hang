//Animation and calcu cash
let dropdown = document.getElementsByClassName('dropdown-btn');
let productResult = $('#productResult');
let searchProduct = $('#searchProduct');

for (let i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener('click', function () {
        let dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === 'block') {
            dropdownContent.style.display = 'none';
            dropdownContent.style.transform = '.2s linear';
        } else {
            dropdownContent.style.display = 'block';
        }
    });
}

//Show result when focus
searchProduct.focus(() => {
    productResult.css('height', '200px');
});

searchProduct.focusout(() => {
    productResult.css('height', '0');
});
