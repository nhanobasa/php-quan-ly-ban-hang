$(function () {
    /**
     * 
     *  address: "Hanoi"
        created: "2020-07-02 04:49:25"
        date: "1999-06-10"
        email: "nhan.itptit@gmail.com"
        id: "1"
        name: "Trần Đức Nhân"
        phone: "0398081681"
        sex: "male"
     */

    /**
     *
     * @param {*} item
     */

    let createTag = (item) => {
        return `<tr class="tr">
                <td><input type="checkbox" value=${item.id} class="select_checkbox" onclick="delCheckBox()"></td>
                <th class="customer-name" scope="row" id=${item.id} onclick="window.location.href='/Customers/editCustomer.html?id=${item.id}'">${item.id}</th>
                <td onclick="window.location.href='/Customers/editCustomer.html?id=${item.id}'"><a href="/Customers/editCustomer.html?id=${item.id}" >${item.name}</a></td>
                <td onclick="window.location.href='/Customers/editCustomer.html?id=${item.id}'">${item.email}</td>                
                <td onclick="window.location.href='/Customers/editCustomer.html?id=${item.id}'">${item.phone}</td>
                <td onclick="window.location.href='/Customers/editCustomer.html?id=${item.id}'">${item.city}</td>
            </tr>`;
    };

    let token = localStorage.getItem('access_token');
    let request = $.get({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/customer',
    });

    request.done(function (response, textStatus, jqXHR) {
        // console.log(response);
        response.body.map(function (item) {
            $('#customers_list').append(createTag(item));
        });
    });
    request.fail(function (jqXHR, textStatus) {
        error = jqXHR.responseJSON.error;
        if (error != 'Unauthorized') {
            window.location.href = '/login';
        } else {
            $('#list-customer').addClass('alert-authorized');
            $('#list-customer').html(
                "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
            );
            $('#list-customer').append(
                '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
            );
        }
    });
});
