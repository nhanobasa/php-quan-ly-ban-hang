function editCustomer(event) {
  event.preventDefault();

  let data = {
    id: GetURLParameter("id"),
    name: $("#input-name").val(),
    phone: $("#input-phone").val(),
    email: $("#input-email").val(),
    address: $("#input-address").val(),
    city: $("#input-city option:selected").text(),
    district: $("#input-district option:selected").text(),
    date: $("#input-date").val(),
    sex: $("#input-sex").val(),
  };

  if (data["name"] === "")
    return false;

  if (!validateEmail(data.email))
    return false;

  if (!validatePhonenumber(data.phone))
    return false;
  let token = localStorage.getItem("access_token");
  let request_edit = $.ajax({
    type: "PUT",
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    url: "http://127.0.0.1/cnpm/backend/api/editcustomer",
    dataType: "json",
    data: JSON.stringify(data), // access in body
  });

  request_edit.done(function (response, textStatus, jqXHR) {
    console.log(response);
    $(".alert").text("Cập nhật thông tin khách hàng thành công");
    $(".alert").removeClass("hidden");
    setTimeout(function () {
      $(".alert").addClass("hidden");
      window.location.href = "/Customers/";
    }, 3000);
  });
  request_edit.fail(function (jqXHR, textStatus) {
    console.log(jqXHR);

    $(".alert").text(`${jqXHR.responseJSON["error"]}`);
    $(".alert").removeClass("hidden");
    $(".alert").addClass("alert-danger");
    setTimeout(function () {
      $(".alert").addClass("hidden");
    }, 3000);
    setTimeout(function () {
      $(".alert").removeClass("alert-danger");
    }, 8000);
  });
}

function GetURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split("&");
  for (var i = 0; i < sURLVariables.length; i++) {
    var sParameterName = sURLVariables[i].split("=");
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    }
  }
}
