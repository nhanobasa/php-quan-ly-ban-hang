$(".input-group.date").datepicker({
	format: "dd-mm-yyyy",
});
$(".input-group.date").datepicker("setDate", new Date("2000-01-01"));

function createCustomer(event) {
	event.preventDefault();

	let data = {
		name: $("#input-name").val(),
		phone: $("#input-phone").val(),
		email: $("#input-email").val(),
		address: $("#input-address").val(),
		city:
			$("#input-city option:selected").val() == ""
				? ""
				: $("#input-city option:selected").text(),
		district:
			$("#input-district option:selected").val() == ""
				? ""
				: $("#input-district option:selected").text(),
		date: $("#input-date").val(),
		sex: $("#input-sex").val() == "Giới tính" ? null : $("#input-sex").val(),
	};

	if (data.name === "") {
		alertError("Hãy điền tên của bạn!");
		return false;
	}

	if (!validateEmail(data.email)) {
		return false;
	}

	if (!validatePhonenumber(data.phone)) {
		return false;
	}

	let request_add = $.post({
		headers: {
			'Authorization': `Bearer ${token}`,
		},
		url: "http://127.0.0.1/cnpm/backend/api/addCustomer",
		dataType: "json",
		data: data, // access in body
	});

	request_add.done(function (response, textStatus, jqXHR) {
		console.log(response);
		alertSuccess("Thêm khách hàng thành công.", "/customers/");
	});
	request_add.fail(function (jqXHR, textStatus) {
		let error = jqXHR.responseJSON.error;
		if (jqXHR.status === 500) {
			alertError(error);
		} else if (error != "Unauthorized") {
			window.location.href = "/login"
		} else {
			$("#list-customer")
				.html("<h1 style='color:#red'>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>");
		}

	});
}

function GetURLParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split("&");
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split("=");
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}
