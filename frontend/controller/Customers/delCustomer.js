function checkAll() {
    let baseCheck = $('#check-all').is(':checked');
    // console.log(baseCheck);

    $('.select_checkbox').each(function () {
        $(this).prop('checked', baseCheck);
    });
}

let countChecked = function () {
    let n = $('.select_checkbox:checked').length;
    return n;
};

$('input[type=checkbox]').on('click', countChecked);

function delCheckBox() {
    let count = countChecked();
    if (count > 0) $('#delele-button').addClass('show');
    else $('#delele-button').removeClass('show');
}

function delButton() {
    let category_id_array = [];
    $('.select_checkbox:checked').each(function () {
        category_id_array.push($(this).val());
    });
    console.log(JSON.stringify({ id: category_id_array }));
    let token = localStorage.getItem('access_token');
    let request_del = $.ajax({
        type: 'DELETE',
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/delcustomer',
        dataType: 'json',
        data: JSON.stringify({ id: category_id_array }),
    });

    request_del.done(function (response, textStatus, jqXHR) {
        console.log(response);
        alertSuccess(
            `Xóa ${category_id_array.length} danh mục thành công`,
            '/Products/category.html'
        );
    });
    request_del.fail(function (jqXHR, textStatus) {
        alert(textStatus);
    });
}
