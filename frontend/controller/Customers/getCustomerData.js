function getCustomerData() {
    let citySelected = $('#input-city');
    let districtSelected = $('#input-district');

    let id = GetURLParameter('id');
    let token = localStorage.getItem('access_token');
    if (id) {
        let request = $.get({
            headers: {
                Authorization: `Bearer ${token}`,
            },
            url: 'http://127.0.0.1/cnpm/backend/api/customer',
            data: {
                id: id,
            },
        });

        request.done(function (response, textStatus, jqXHR) {
            let body = response.body[0];
            // data mẫu
            /**
             *  address: "Hanoi"
                city: null
                created: "2020-07-02 10:20:40"  
                date: "1999-06-10"
                district: null
                email: "quangnguyen@gmail.com"
                id: "6" 
                name: "Nguyễn Văn Quang"
                phone: "0398081682"
                sex: "male"
             */
            $('#input-name').val(body['name']);
            $('#input-phone').val(body['phone']);
            $('#input-email').val(body['email']);
            $('#input-address').val(body['address']);
            $('#input-date').val(body['date']);
            $('#input-sex').val(body['sex']);

            let date_array = body['date'].split('-');

            $('.input-group.date').datepicker({
                format: 'dd-mm-yyyy',
                defaultViewDate: {
                    year: date_array[2],
                    month: date_array[1],
                    day: date_array[0],
                },
            });

            let cityID;
            $('select#input-city option').each(function (index, element) {
                if (element.text === body['city']) {
                    cityID = element.value;
                    element.setAttribute('selected', true);
                }
            });
            $.get({
                url: districtAPI(cityID),
            }).done((data) => {
                data.map((district) => {
                    districtSelected.append(createTag(district));
                });
                $('select#input-district option').each(function (
                    index,
                    element
                ) {
                    if (element.text === body['district']) {
                        // console.log(element);

                        cityID = element.value;
                        element.setAttribute('selected', true);
                    }
                });
            });
        });
        request.fail(function (jqXHR, textStatus) {
            error = jqXHR.responseJSON.error;
            if (error != 'Unauthorized') {
                window.location.href = '/login';
                localStorage.clear();
            } else {
                $('#list-customer').addClass('alert-authorized');
                $('#list-customer').html(
                    "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
                );
                $('#list-customer').append(
                    '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
                );
            }
        });
    }
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
