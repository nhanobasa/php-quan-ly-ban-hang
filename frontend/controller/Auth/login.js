function login(event) {
    event.preventDefault();

    data = {
        email: $("#input-email").val(),
        password: $("#input-password").val(),
    };

    
    if (!validateEmail(data.email)) {
        return false;
    }

    if(data.password === ""){
        return false;
    }
    console.log(data);
    let request = $.ajax({
        type: "post",
        url: "http://127.0.0.1/cnpm/backend/api/login",
        data: data,
    });
    
    request.done(function (response, textStatus, jqXHR) {
        // Log a message to the console
        console.log("Login OK!");
        console.log(response);
        localStorage.setItem("access_token",response.access_token);
        localStorage.setItem("profile",JSON.stringify(response.profile));
        window.location.href = "/home"
    });
    request.fail(function (jqXHR, textStatus) {
        console.log(jqXHR.responseJSON);
        alertError("Email hoặc mật khẩu không chính xác.")
    });
}