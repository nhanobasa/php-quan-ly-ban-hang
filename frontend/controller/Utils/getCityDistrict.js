let citySelected = $("#input-city");
let districtSelected = $("#input-district");
let cityAPI = "https://thongtindoanhnghiep.co/api/city";
let districtAPI = (cityID) => {
    return `https://thongtindoanhnghiep.co/api/city/${cityID}/district`;
};

let getCity = $.get({
    url: cityAPI
});

let createTag = (obj) => {
    return `<option value="${obj.ID}">${obj.Title}</option>`;
};

getCity.done(data => {
        data.LtsItem.map(city => {
        citySelected.append(createTag(city));    
    });
    if(GetURLParameter("id")){
        getCustomerData();
    }
    
});
citySelected.change(function () {
    districtSelected.empty();
    districtSelected.append(`<option value="">Quận/Huyện</option>`);
    let citytID = citySelected.val();
    $.get({
        url: districtAPI(citytID)
    }).done(data => {
        data.map(district => {
            districtSelected.append(createTag(district));
        });                        
    });
})