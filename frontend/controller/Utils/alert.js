function alertSuccess(msg, redirectUrl) {
    $('.alert').text(msg);
    $('.alert').removeClass('hidden');
    setTimeout(function () {
        $('.alert').addClass('hidden');
        window.location.href = redirectUrl;
    }, 3000);
}

function alertError(msg) {
    $('.alert').text(msg);
    $('.alert').removeClass('hidden');
    $('.alert').addClass('alert-danger');
    setTimeout(function () {
        $('.alert').addClass('hidden');
    }, 3000);
    setTimeout(function () {
        $('.alert').removeClass('alert-danger');
    }, 9000);
}
