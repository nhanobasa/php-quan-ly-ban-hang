$(function () {
    let timeout = null;
    let inputSearchCategory = $('#product_category');
    inputSearchCategory.focus(function () {
        search_category();
        $('.search-category').addClass('show-editor');
        inputSearchCategory.keyup(function () {
            // Xóa đi những gì ta đã thiết lập ở sự kiện
            // keyup của ký tự trước (nếu có)
            clearTimeout(timeout);
            // Sau khi xóa thì thiết lập lại timeout
            timeout = setTimeout(function () {
                // Lấy nội dung search
                let query = inputSearchCategory.val();

                // Gửi ajax search
                console.log(query);
                search_category(query);
            }, 500);
        });
    });
    inputSearchCategory.focusout(function () {
        setTimeout(function () {
            $('.search-category').removeClass('show-editor');
            $('#search-category-body').empty();
        }, 500);
    });
});

function select_result(category_id, category_name) {
    console.log(category_id);
    console.log(category_name);
    $('#product_category').val(category_name);
    $('#product_category').data('category_id', category_id);
}

function search_category(query = '') {
    query = query.trim();
    url = `http://127.0.0.1/cnpm/backend/api/searchCategory?q=${query}`;
    if (query == '') {
        url = `http://127.0.0.1/cnpm/backend/api/category`;
    }

    let request_search = $.get({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: url,
    });
    request_search.done(function (response, textStatus, jqXHR) {
        console.log(response);
        $('#search-category-body').empty();
        if (response.msg) {
            $('#search-category-body').append(
                `<li class="search-category-item" >Không tìm thấy danh mục</li>`
            );
        } else {
            response.body.map(function (item) {
                $('#search-category-body').append(
                    `<li class="search-category-item" onclick="select_result('${item.category_id}','${item.category_name}')">${item.category_name}</li>`
                );
            });
        }
    });
    request_search.fail(function (jqXHR, textStatus) {
        console.log(jqXHR);
        let statusCode = jqXHR.status;

        error = jqXHR.responseJSON.error;
        if (error != 'Unauthorized' && statusCode == 401) {
            window.location.href = '/login';
        } else {
            $('.content').addClass('alert-authorized');
            $('.content').html(
                "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
            );
            $('.content').append(
                '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
            );
        }
    });
}
