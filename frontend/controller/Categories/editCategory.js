function editCategory(event) {
    event.preventDefault();
    let category_id = $('.modal-body').data('currentid');
    console.log('current_id: ' + category_id);
    let category_name = $('.modal-body input#recipient-name').val();
    console.log(category_name);
    let request_edit = $.ajax({
        type: 'PUT',
        url: 'http://127.0.0.1/cnpm/backend/api/category',
        dataType: 'json',
        data: JSON.stringify({
            category_id: category_id,
            category_name: category_name,
        }),
    });
    request_edit.done(function (response, textStatus, jqXHR) {
        console.log(response);
        $('.alert').text('Cập nhật danh mục thành công');
        $('.alert').removeClass('hidden');
        setTimeout(function () {
            $('.alert').addClass('hidden');
            window.location.href = '/Products/category.html';
        }, 3000);
    });
    request_edit.fail(function (jqXHR, textStatus) {
        console.log(jqXHR);

        $('.alert').text(`${jqXHR.responseJSON['error']}`);
        $('.alert').removeClass('hidden');
        $('.alert').addClass('alert-danger');
        setTimeout(function () {
            $('.alert').addClass('hidden');
        }, 3000);
        setTimeout(function () {
            $('.alert').removeClass('alert-danger');
        }, 8000);
    });
}
