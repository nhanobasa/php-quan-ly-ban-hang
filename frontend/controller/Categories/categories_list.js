$(function () {
    // Lấy dữ liệu danh sách danh mục sản phẩm
    // let token = localStorage.getItem('access_token');
    let request = $.get({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/category',
    });

    request.done(function (response, textStatus, jqXHR) {
        // console.log(response);
        response.body.map(function (item) {
            item.checkBox = `<input type="checkbox" value=${item.category_id} class="select_checkbox" onclick="delCheckBox()">`;
            if (item.category_id != 0) {
                item.editBtn = `<button type="button" class="btn btn-warning" data-toggle="modal" 
                                data-target="#editModal" data-id="${item.category_id}" data-name="${item.category_name}">
                                Sửa
                                </button>`;
            } else {
                item.editBtn = '';
            }
        });
        $('#dataTable').DataTable({
            data: response.body,
            columns: [
                { data: 'checkBox' },
                { data: 'category_id' },
                { data: 'category_name' },
                { data: 'total_product' },
                { data: 'created' },
                { data: 'editBtn' },
            ],
            fixedHeader: {
                headerOffset: $('.profile-navbar').outerHeight(),
            },
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            paging: false,
        });
        $(
            '#dataTable_filter'
        ).append(` <button type="button" class="btn btn-primary" data-toggle="modal" 
                                data-target="#addCategoryModal" style="margin-left: 2px; margin-top: -8px;">Thêm danh mục</button>
                <button type="button" class="btn btn-danger" id="delele-button"
                            style="display: none; margin-top: -8px;" data-toggle="modal" data-target="#exampleModal" >Xóa</button>
                            `);
    });
    request.fail(function (jqXHR, textStatus) {
        console.log(jqXHR);
        let statusCode = jqXHR.status;

        error = jqXHR.responseJSON.error;
        if (error != 'Unauthorized' && statusCode == 401) {
            window.location.href = '/login';
        } else {
            $('.content').addClass('alert-authorized');
            $('.content').html(
                "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
            );
            $('.content').append(
                '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
            );
        }
    });

    // Call the dataTables jQuery plugin
});
