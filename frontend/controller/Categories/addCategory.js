function addCategory(event) {
    event.preventDefault();
    console.log('Thêm danh mục');
    let category_name = $('#category-name').val();
    if (category_name.length == 0) {
        $('.needs-validation').addClass('was-validated');
        return false;
    }
    let request_add = $.post({
        url: 'http://127.0.0.1/cnpm/backend/api/category',
        dataType: 'json',
        data: JSON.stringify({ category_name: category_name }),
    });
    request_add.done(function (response, textStatus, jqXHR) {
        console.log(response);
        alertSuccess(`Thêm danh mục thành công`, '/Products/category.html');
    });
    request_add.fail(function (jqXHR, textStatus) {
        console.log(jqXHR.responseJSON);
        let error = jqXHR.responseJSON.error;

        alertError(error, '/Products/category.html');
    });
}
