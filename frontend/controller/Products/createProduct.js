let product_name = $('#product_name');
let product_sku = $('#product_sku');
let product_description = $('#product_description');
let product_price = $('#product_price');
let product_promotion_price = $('#product_promotion_price');
let product_entry_price = $('#product_entry_price');

let product_stock_quantity = $('#product_stock_quantity');
let product_can_sale = $('#product_can_sale');
let product_low_stock = $('#product_low_stock');
let product_category = $('#product_category');
let preview = document.querySelector('#proImg_preview');
let base64Img;

previewFile = () => {
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.onload = function () {
        // convert image file to base64 string
        base64Img = reader.result;
        preview.src = reader.result;
    };
    reader.readAsDataURL(file);
};
//===========================================================
//===========================================================

function createProduct(event) {
    event.preventDefault();
    let data = {
        product_name: product_name.val(),
        sku: product_sku.val(),
        category_id:
            product_category.data('category_id') !== ''
                ? product_category.data('category_id')
                : 0,
        description: product_description.val(),
        price: product_price.val(),
        promotion_price: product_promotion_price.val(),
        entry_price: product_entry_price.val(),
        stock_quantity: product_stock_quantity.val(),
        can_sale: product_can_sale.val(),
        low_stock: product_low_stock.val(),
        total_sale: null,
        image: base64Img,
    };
    console.log(data);

    if (data.product_name === '') {
        return false;
    }
    if (data.price === '') {
        return false;
    }
    if (data.entry_price === '') {
        return false;
    }

    let token = localStorage.getItem('access_token');
    let request_add = $.post({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/product',
        dataType: 'json',
        data: JSON.stringify(data),
    });
    request_add.done(function (response, textStatus, jqXHR) {
        console.log(response);
        alertSuccess('Thêm sản phẩm thành công.', '/products/');
    });
    request_add.fail(function (jqXHR, textStatus) {
        console.log(jqXHR);
        let error = jqXHR.responseJSON.error;
        if (jqXHR.status === 500) {
            alertError(error);
        } else if (error != 'Unauthorized' && jqXHR.status == 401) {
            window.location.href = '/login';
        } else {
            $('.content').html(
                "<h1 style='color:#red'>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>"
            );
        }
    });
}
