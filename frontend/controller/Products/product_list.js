$(function () {
    let token = localStorage.getItem('access_token');
    // Lấy dữ liệu danh sách danh mục sản phẩm
    let request = $.get({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/product',
    });

    request.done(function (response, textStatus, jqXHR) {
        console.log(response);
        // debugger;
        response.body.map(function (item) {
            item.checkBox = `<input type="checkbox" value=${item.product_id} class="select_checkbox" onclick="delCheckBox()">`;

            item.editBtn = `<button type="button" class="btn btn-warning"
                                data-product = ${item}"
                                onclick="window.location.href='/Products/editProduct.html?product_id=${item.product_id}'">
                                Sửa
                                </button>`;
            if (item.image != '') {
                item.image = `<img src="http://127.0.0.1/cnpm/backend/${item.image}"
                                id="proImg_preview"
                                height="50px"
                        />`;
            }
        });
        $('#dataTable').DataTable({
            data: response.body,
            columns: [
                { data: 'checkBox' },
                { data: 'product_id' },
                { data: 'image' },
                { data: 'product_name' },
                { data: 'sku' },
                { data: 'category_name' },
                { data: 'price' },
                { data: 'promotion_price' },
                { data: 'entry_price' },
                { data: 'can_sale' },
                { data: 'stock_quantity' },
                { data: 'created' },
                { data: 'editBtn' },
            ],
            fixedHeader: {
                headerOffset: $('.profile-navbar').outerHeight(),
            },
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            paging: false,
        });
        $(
            '#dataTable_filter'
        ).append(`  <a href="/Products/createProduct.html" class="btn btn-primary btn-col-2"
                     style="margin-left: 2px; margin-top: -8px;">Thêm sản phẩm</a>
                     
                    <button type="button" class="btn btn-danger" id="delele-button"
                            style="display: none; margin-top: -8px;" data-toggle="modal" data-target="#exampleModal">Xóa</button>
                            `);
    });
    request.fail(function (jqXHR, textStatus) {
        console.log(jqXHR);
        let statusCode = jqXHR.status;

        error = jqXHR.responseJSON.error;
        if (error != 'Unauthorized' && statusCode == 401) {
            window.location.href = '/login';
        } else {
            $('.content').addClass('alert-authorized');
            $('.content').html(
                "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
            );
            $('.content').append(
                '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
            );
        }
    });

    // Call the dataTables jQuery plugin
});
