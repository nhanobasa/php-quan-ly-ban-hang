let product_name = $('#product_name');
let product_sku = $('#product_sku');
let product_description = $('#product_description');
let product_price = $('#product_price');
let product_promotion_price = $('#product_promotion_price');
let product_entry_price = $('#product_entry_price');

let product_stock_quantity = $('#product_stock_quantity');
let product_can_sale = $('#product_can_sale');
let product_low_stock = $('#product_low_stock');
let product_category = $('#product_category');
let preview = document.querySelector('#proImg_preview');
let base64Img;

previewFile = () => {
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.onload = function () {
        // convert image file to base64 string
        base64Img = reader.result;
        preview.src = reader.result;
    };
    reader.readAsDataURL(file);
};
//===========================================================

$(function () {
    let product_id = GetURLParameter('product_id');
    let token = localStorage.getItem('access_token');
    if (product_id) {
        let request = $.get({
            headers: {
                Authorization: `Bearer ${token}`,
            },
            url: 'http://127.0.0.1/cnpm/backend/api/product',
            data: {
                product_id: product_id,
            },
        });

        request.done(function (response, textStatus, jqXHR) {
            data = response.body[0];
            console.log(data);
            product_name.val(data.product_name);
            product_sku.val(data.sku);
            product_description.val(data.description);
            product_price.val(data.price);
            product_promotion_price.val(data.promotion_price);
            product_entry_price.val(data.entry_price);
            product_stock_quantity.val(data.stock_quantity);
            product_can_sale.val(data.can_sale);
            product_category.val(data.category_name);
            product_category.data('category_id', data.category_id);
            product_low_stock.val(data.low_stock);
            if (data.image != '') {
                preview.src = `http://127.0.0.1/cnpm/backend/${data.image}`;
            } else {
                preview.src = '';
            }
        });
        request.fail(function (jqXHR, textStatus) {
            console.log(jqXHR);

            // error = jqXHR.responseJSON.error;
            // if (error != 'Unauthorized') {
            //     window.location.href = '/login';
            //     localStorage.clear();
            // } else {
            //     $('#list-customer').addClass('alert-authorized');
            //     $('#list-customer').html(
            //         "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
            //     );
            //     $('#list-customer').append(
            //         '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
            //     );
            // }
        });
    }
});

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function toDataUrl(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        var reader = new FileReader();
        reader.onloadend = function () {
            callback(reader.result);
        };
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}
