function editProduct(event) {
    event.preventDefault();

    let base64Img = '';
    console.log(preview.src);
    console.log(window.location.href);

    toDataUrl(preview.src, function (myBase64) {
        debugger;
        if (preview.src !== window.location.href) {
            base64Img = myBase64;
        }

        let data = {
            product_id: GetURLParameter('product_id'),
            product_name: product_name.val(),
            sku: product_sku.val(),
            category_id:
                product_category.data('category_id') !== ''
                    ? product_category.data('category_id')
                    : 0,
            description: product_description.val(),
            price: product_price.val(),
            promotion_price: product_promotion_price.val(),
            entry_price: product_entry_price.val(),
            stock_quantity: product_stock_quantity.val(),
            can_sale: product_can_sale.val(),
            low_stock: product_low_stock.val(),
            total_sale: null,
            image: base64Img,
        };

        console.log(data);

        //  Ajax request to the server
        let request_edit = $.ajax({
            type: 'PUT',
            url: 'http://127.0.0.1/cnpm/backend/api/product',
            dataType: 'json',
            data: JSON.stringify(data),
        });
        request_edit.done(function (response, textStatus, jqXHR) {
            console.log(response);
            alertSuccess('Cập nhật sản phẩm thành công', '/Products');
        });
        request_edit.fail(function (jqXHR, textStatus) {
            console.log(jqXHR);

            $('.alert').text(`${jqXHR.responseJSON['error']}`);
            $('.alert').removeClass('hidden');
            $('.alert').addClass('alert-danger');
            setTimeout(function () {
                $('.alert').addClass('hidden');
            }, 3000);
            setTimeout(function () {
                $('.alert').removeClass('alert-danger');
            }, 8000);
        });
    });
}
