//Animation and calcu cash
let dropdown = document.getElementsByClassName("dropdown-btn");
let productResult = $("#customers_result");
let searchProduct = $("#search_customer");


for (let i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        let dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}

//Show result when focus
searchProduct.focus(() => {
    productResult.css("height", "200px");
});

searchProduct.focusout(() => {
    productResult.css("height", "0");
});

