let dropdown = document.getElementsByClassName('dropdown-btn');

for (let i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener('click', function () {
        let dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === 'block') {
            dropdownContent.style.display = 'none';
        } else {
            dropdownContent.style.display = 'block';
        }
    });
}
function number_format(number, decimals, dec_point, thousands_sep) {
    // *     example: number_format(1234.56, 2, ',', ' ');
    // *     return: '1 234,56'
    number = (number + '').replace(',', '').replace(' ', '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = typeof thousands_sep === 'undefined' ? ',' : thousands_sep,
        dec = typeof dec_point === 'undefined' ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

$(function () {
    let token = localStorage.getItem('access_token');
    // Lấy dữ liệu danh sách danh mục sản phẩm
    let request = $.get({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/category',
    });

    request.done(function (response, textStatus, jqXHR) {
        console.log(response);

        dataPie = {
            labels: [],
            datasets: [
                {
                    data: [],
                    backgroundColor: [
                        'rgba(255, 99, 132)',
                        'rgba(54, 162, 235)',
                        'rgba(255, 206, 86)',
                        'rgba(75, 192, 192)',
                        'rgba(153, 102, 255)',
                        'rgba(255, 159, 64)',
                        'rgba(255, 159, 84)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 159, 84,1)',
                    ],
                    borderWidth: 1,
                },
            ],
        };
        response.body.map(function (item) {
            if (item.total_product > 0) {
                dataPie.labels.push(item.category_name);
                dataPie.datasets[0].data.push(item.total_product);
            }
        });
        $('#total_category').text(response.itemCount);

        // Pie Chart Tỉ lệ sản phẩm theo danh mục
        var ctx = document.getElementById('myPieChart');

        var myDoughnutChart = new Chart(ctx, {
            type: 'doughnut',
            data: dataPie,
            options: {
                rtl: true,
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: 'rgb(255,255,255)',
                    bodyFontColor: '#858796',
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: true,
                    caretPadding: 10,
                },
                legend: {
                    display: true,
                    position: 'bottom',
                },
                cutoutPercentage: 50,
            },
        });
    });
    request.fail(function (jqXHR, textStatus) {
        error = jqXHR.responseJSON.error;
        if (error != 'Unauthorized') {
            window.location.href = '/login';
        } else {
            $('#list-customer').addClass('alert-authorized');
            $('#list-customer').html(
                "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
            );
            $('#list-customer').append(
                '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
            );
        }
    });
    // =================================================================================================
    // Lấy dữ liệu danh sách danh mục sản phẩm
    let request_get_product = $.get({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/product',
    });

    request_get_product.done(function (response, textStatus, jqXHR) {
        console.log(response);
        // Hiển thị tổng số sản phẩm (card-body)
        $('#total_product').text(response.itemCount);
        let total_stock_quantity = 0; // Tổng sản phẩm tồn kho
        let total_product_can_sale = 0; // Tổng sản phẩm có thể bán

        let dataBar = {
            datasets: [
                {
                    label: 'Tồn kho',
                    data: [],
                    backgroundColor: [
                        'rgba(255, 99, 132)',
                        'rgba(255, 99, 132)',
                        'rgba(255, 99, 132)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 99, 132, 1)',
                    ],
                    borderWidth: 1,
                },
                {
                    label: 'Có thể bán',
                    data: [],
                    backgroundColor: [
                        'rgba(54, 162, 235)',
                        'rgba(54, 162, 235)',
                        'rgba(54, 162, 235)',
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                    ],
                    borderWidth: 1,
                },
                {
                    label: 'Ngưỡng hết hàng',
                    data: [],
                    backgroundColor: [
                        'rgba(255, 206, 86)',
                        'rgba(255, 206, 86)',
                        'rgba(255, 206, 86)',
                    ],
                    borderColor: [
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)',
                    ],
                    borderWidth: 1,
                },
            ],
            labels: [],
        };
        let dataLine = {
            labels: [],
            datasets: [
                {
                    label: 'Gía nhập vào',
                    lineTension: 0.3,
                    backgroundColor: 'rgba(78, 115, 223, 0.05)',
                    borderColor: 'rgba(78, 115, 223, 1)',
                    pointRadius: 3,
                    pointBackgroundColor: 'rgba(78, 115, 223, 1)',
                    pointBorderColor: 'rgba(78, 115, 223, 1)',
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: 'rgba(78, 115, 223, 1)',
                    pointHoverBorderColor: 'rgba(78, 115, 223, 1)',
                    pointHitRadius: 10,
                    pointBorderWidth: 2,
                    data: [],
                },
                {
                    label: 'Gía bán thường',
                    lineTension: 0.3,
                    backgroundColor: 'rgba(75, 192, 192, 0.05)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    pointRadius: 3,
                    pointBackgroundColor: 'rgba(75, 192, 192, 1)',
                    pointBorderColor: 'rgba(75, 192, 192, 1)',
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: 'rgba(75, 192, 192, 1)',
                    pointHoverBorderColor: 'rgba(75, 192, 192, 1)',
                    pointHitRadius: 10,
                    pointBorderWidth: 2,
                    data: [],
                },
                {
                    label: 'Gía khuyến mại',
                    lineTension: 0.3,
                    backgroundColor: 'rgba(255, 99, 132, 0.05)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    pointRadius: 3,
                    pointBackgroundColor: 'rgba(255, 99, 132, 1)',
                    pointBorderColor: 'rgba(255, 99, 132, 1)',
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: 'rgba(255, 99, 132, 1)',
                    pointHoverBorderColor: 'rgba(255, 99, 132, 1)',
                    pointHitRadius: 10,
                    pointBorderWidth: 2,
                    data: [],
                },
            ],
        };
        response.body.map(function (item) {
            if (item.can_sale && item.low_stock && item.stock_quantity) {
                total_product_can_sale += parseInt(item.can_sale);
                total_stock_quantity += parseInt(item.stock_quantity);
                // Xử lý dữ liệu cho bar chart
                if (
                    item.can_sale - item.low_stock <= 50 ||
                    item.can_sale / item.stock_quantity <= 0.5
                ) {
                    dataBar.datasets[0].data.push(item.stock_quantity);
                    dataBar.datasets[0].backgroundColor.push(
                        'rgba(255, 99, 132)'
                    );
                    dataBar.datasets[0].borderColor.push(
                        'rgba(255, 99, 132, 1)'
                    );

                    dataBar.datasets[1].data.push(item.can_sale);
                    dataBar.datasets[1].backgroundColor.push(
                        'rgba(54, 162, 235)'
                    );
                    dataBar.datasets[1].borderColor.push(
                        'rgba(54, 162, 235, 1)'
                    );

                    dataBar.datasets[2].data.push(item.low_stock);
                    dataBar.datasets[2].backgroundColor.push(
                        'rgba(255, 206, 86)'
                    );
                    dataBar.datasets[2].borderColor.push(
                        'rgba(255, 206, 86, 1)'
                    );

                    dataBar.labels.push(item.product_name);
                }
            }

            // Xử lý dữ liệu cho line chart
            dataLine.labels.push(item.product_name);
            dataLine.datasets[0].data.push(item.entry_price);
            dataLine.datasets[1].data.push(item.price);
            dataLine.datasets[2].data.push(item.promotion_price);
        });
        console.log(dataBar);

        console.log(total_product_can_sale);
        console.log(total_stock_quantity);
        let can_sale_percent = (
            (total_product_can_sale / total_stock_quantity) *
            100
        ).toFixed(2);
        console.log(can_sale_percent);

        $('#can_sale_percent div.result_value').text(can_sale_percent + '%');
        $('#can_sale_percent div.progress-bar').css(
            'width',
            can_sale_percent + '%'
        );

        // Bar Chart Tỉ lệ sản phẩm theo danh mục
        var ctx = document.getElementById('myBarChart');
        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: dataBar,
            options: {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                min: 0,
                                maxTicksLimit: 20,
                            },
                        },
                    ],
                },
            },
        });
        // Line Chart thống kê giá sản phẩm
        var ctx = document.getElementById('myLineChart');
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: dataLine,
            options: {
                maintainAspectRatio: true,
                layout: {
                    padding: {
                        left: 10,
                        right: 25,
                        top: 25,
                        bottom: 0,
                    },
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: true,
                                drawBorder: true,
                            },
                            ticks: {
                                // maxTicksLimit: 7,
                            },
                        },
                    ],
                    yAxes: [
                        {
                            ticks: {
                                // maxTicksLimit: 5,
                                padding: 10,
                                // Include a dollar sign in the ticks
                                callback: function (value, index, values) {
                                    return number_format(value) + 'đ';
                                },
                            },
                            gridLines: {
                                color: 'rgb(234, 236, 244)',
                                zeroLineColor: 'rgb(234, 236, 244)',
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2],
                            },
                        },
                    ],
                },
                legend: {
                    display: false,
                },
                tooltips: {
                    backgroundColor: 'rgb(255,255,255)',
                    bodyFontColor: '#858796',
                    titleMarginBottom: 10,
                    titleFontColor: '#6e707e',
                    titleFontSize: 14,
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    intersect: false,
                    mode: 'index',
                    caretPadding: 10,
                    callbacks: {
                        label: function (tooltipItem, chart) {
                            var datasetLabel =
                                chart.datasets[tooltipItem.datasetIndex]
                                    .label || '';
                            return (
                                datasetLabel +
                                ':' +
                                number_format(tooltipItem.yLabel) +
                                'đ'
                            );
                        },
                    },
                },
            },
        });
    });
    request_get_product.fail(function (jqXHR, textStatus) {
        error = jqXHR.responseJSON.error;
        if (error != 'Unauthorized') {
            window.location.href = '/login';
        } else {
            $('#list-customer').addClass('alert-authorized');
            $('#list-customer').html(
                "<img src='/images/authorized.svg' alt='Kiwi standing on oval'style='max-height: 80%; height: 80%;opacity: 0.5;'></img>"
            );
            $('#list-customer').append(
                '<h1>Bạn không có quyền truy cập! Hãy liên hệ chủ shop để được cấp quyền.</h1>'
            );
        }
    });
    // =================================================================================================
    // Lấy dữ liệu danh sách danh mục sản phẩm
    let request_get_user = $.get({
        headers: {
            Authorization: `Bearer ${token}`,
        },
        url: 'http://127.0.0.1/cnpm/backend/api/user',
    });

    request_get_user.done(function (response, texuser, jqXHR) {
        // console.log(response);
        $('#total_user').text(response.itemCount);
    });
    request_get_user.fail(function (jqXHR, jqXusertStatus) {
        error = jqXHR.responseJSON.error;
        if (error != 'Unauthorized' && jqXHR.status == 401) {
            window.location.href = '/login';
        } else {
            $('#total_user').text('-');
        }
    });
});
